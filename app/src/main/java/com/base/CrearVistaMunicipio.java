package com.base;

import android.os.Bundle;
import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
/*
    * Calse para mostrar la lista de municipio*/

public class CrearVistaMunicipio extends Activity {

    private RecyclerView recyclerViewMunicipio; /*lista de municipios*/

    ArrayList<Municipio> municipios;  //vector con lista de municipios


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_vista_municipio);

        recyclerViewMunicipio = (RecyclerView) findViewById(R.id.recycler_municipio);

            // crer lista de elementos  a mostarr en lista desde BD

        CrearBD base = new CrearBD(this, "BasePrueba", null, 1);
        municipios = base.getMunicipios();

        AdaptadorMunicipio adapter = new AdaptadorMunicipio(municipios);
            // aplicar adaptador al recycler view actual
        recyclerViewMunicipio.setAdapter(adapter);
        recyclerViewMunicipio.setLayoutManager(new LinearLayoutManager(this));


    }

}
