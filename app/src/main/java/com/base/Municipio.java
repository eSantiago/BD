package com.base;

import java.util.ArrayList;

/**
 * Created by stn on 18/09/16.
 * Clase para almacenar en arreglo los datos de los municipios
 * Establecer y obtener eelementos de dicho arreglo desde otras clases
 */
public class Municipio {
    private String nombreMunicipio;

    Municipio(){                    //Constructor para usarse cuando se crea la instancia sin enviar lista a integrar en nombreMunicipio
    }

    Municipio(String nMunicipio){  //Constructor para asignar elementos en variable nombreMunicipio
        this.nombreMunicipio = nMunicipio;
    }

    public String getMunicipios(){      //para obtener valores de nombreMunicipio desde instancia Municipio
        return nombreMunicipio;
    }

    public void setMunicipio(String nMunicipio ){  //para establecer valores de nombreMunicipio desde instancia Municipio
        this.nombreMunicipio = nMunicipio;
    }


}
