package com.base;

import java.util.ArrayList;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;



public class CrearBD extends SQLiteOpenHelper{
	private String crearRegion = "CREATE TABLE Region (idRegion INTEGER PRIMARY KEY ASC, nombreRegion VARCHAR);";
	private String crearMunicipio = "CREATE TABLE Municipio (idMunicipio INTEGER PRIMARY KEY ASC, nombreMunicipio VARCHAR);";
	private String crearSubdireccion = "CREATE TABLE Subdireccion (idSubdireccion INTEGER PRIMARY KEY ASC, nombreSubdireccion VARCHAR, telefono VARCHAR);";
	private String crearVialidad = "CREATE TABLE Vialidad (idVialidad INTEGER PRIMARY KEY ASC, tipoVialidad VARCHAR);";

	private String crearCobertura = "CREATE TABLE Cobertura (idCobertura INTEGER PRIMARY KEY ASC, idSubdireccion INTEGER, idRegion INTEGER, idMunicipio INTEGER, idVialidad INTEGER, FOREIGN KEY (idVialidad) REFERENCES Vialidad(idVialidad),FOREIGN KEY (idRegion) REFERENCES Region(idRegion), FOREIGN KEY (idMunicipio) REFERENCES Municipio(idMunicipio));";
	private String crearInfraccion = "CREATE TABLE Infraccion (idInfraccion INTEGER PRIMARY KEY ASC, tipoVehiculo VARCHAR, motivo VARCHAR, sancion VARCHAR, idVialidad INTEGER, FOREIGN KEY (idVialidad) REFERENCES Vialidad(idVialidad));";
	private String crearPolicia = "CREATE TABLE Policia (idPolicia INTEGER PRIMARY KEY ASC, tipoPolicia VARCHAR, acciones VARCHAR, idVialidad INTEGER, FOREIGN KEY (idVialidad) REFERENCES Vialidad(idVialidad));";

	/*contructor recibe argumentos con informacion para ---crear la BD---*/
public CrearBD(Context contexto, String nombreBD, CursorFactory factory, int version ){
	super(contexto, nombreBD, factory, version);
}

@Override
public void onCreate(SQLiteDatabase bd) {	/*crea las tablas*/
	bd.execSQL(crearRegion);	
	bd.execSQL(crearMunicipio);
	bd.execSQL(crearSubdireccion);
	bd.execSQL(crearVialidad);
	bd.execSQL(crearCobertura);
	bd.execSQL(crearInfraccion);
	bd.execSQL(crearPolicia);
}

@Override
public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	// TODO Auto-generated method stub
	//Se elimina la versión anterior de la tabla
    db.execSQL("DROP TABLE IF EXISTS Municipio");
    

    //Se crea la nueva versión de la tabla
   // db.execSQL(crearBD);
    onCreate(db);
}

public void insertarDatos(){
	//String iMunicipio = "INSERT INTO Municipio (nombreMunicipio) VALUES (\"Acambay\"),(\"Acolman\");";
	String iMunicipio = "INSERT INTO Municipio (nombreMunicipio) VALUES"+
			"(\"	Toluca		\"),"+
			"(\"	Mexicaltzingo		\"),"+
			"(\"	Metepec		\"),"+
			"(\"	Chapultepec		\"),"+
			"(\"	San Mateo Atenco		\"),"+
			"(\"	Lerma		\"),"+
			"(\"	Almoloya del Río		\"),"+
			"(\"	Capulhuac		\"),"+
			"(\"	Ocoyoacac		\"),"+
			"(\"	Tianguistenco		\"),"+
			"(\"	Atizapan Santa Cruz		\"),"+
			"(\"	Xalatlaco		\"),"+
			"(\"	Zinacantepec		\"),"+
			"(\"	Almoloya de Juárez		\"),"+
			"(\"	Xonacatlán		\"),"+
			"(\"	Temoaya		\"),"+
			"(\"	Otzolotepec		\"),"+
			"(\"	Texcalyacac		\"),"+
			"(\"	Tenango del Valle		\"),"+
			"(\"	San Antonio la Isla		\"),"+
			"(\"	Rayón		\"),"+
			"(\"	Calimaya		\"),"+
			"(\"	Ocuilan		\"),"+
			"(\"	Malinalco		\"),"+
			"(\"	Joquicingo		\"),"+
			"(\"	Zumpahuacán		\"),"+
			"(\"	Villa Guerrero		\"),"+
			"(\"	Tenancingo		\"),"+
			"(\"	Malinalco		\"),"+
			"(\"	Ixtapan de la Sal		\"),"+
			"(\"	Coatepec Harinas		\"),"+
			"(\"	Zacualpan		\"),"+
			"(\"	Texcaltitlán		\"),"+
			"(\"	Sultepec		\"),"+
			"(\"	Almoloya de Alquisiras		\"),"+
			"(\"	Luvianos		\"),"+
			"(\"	San Simón de Guerrero		\"),"+
			"(\"	Tejupilco		\"),"+
			"(\"	Temascaltepec		\"),"+
			"(\"	Amatepec		\"),"+
			"(\"	Tlatlaya		\"),"+
			"(\"	Valle de Bravo		\"),"+
			"(\"	Amanalco		\"),"+
			"(\"	Zacazonapan		\"),"+
			"(\"	Santo Tomás de los Plátanos		\"),"+
			"(\"	Otzoloapan		\"),"+
			"(\"	Villa Victoria		\"),"+
			"(\"	Villa de Allende		\"),"+
			"(\"	Ixtapan del Oro		\"),"+
			"(\"	Donato Guerra		\"),"+
			"(\"	Villa del Carbón		\"),"+
			"(\"	Soyaniquilpan		\"),"+
			"(\"	Jilotepec		\"),"+
			"(\"	Chapa de Mota		\"),"+
			"(\"	Acambay		\"),"+
			"(\"	Aculco		\"),"+
			"(\"	Polotitlán		\"),"+
			"(\"	Timilpan		\"),"+
			"(\"	Temascalcingo		\"),"+
			"(\"	Atlacomulco		\"),"+
			"(\"	El Oro		\"),"+
			"(\"	San Felipe del Progreso		\"),"+
			"(\"	San José del Rincón		\"),"+
			"(\"	Morelos		\"),"+
			"(\"	Jocotitlán		\"),"+
			"(\"	Jiquipilco		\"),"+
			"(\"	Ixtlahuaca		\"),"+
			"(\"	Isidro Fabela		\"),"+
			"(\"	Nicolás Romero		\"),"+
			"(\"	Jilotzingo		\"),"+
			"(\"	Naucalpan		\"),"+
			"(\"	Tlalnepantla		\"),"+
			"(\"	Atizapán de Zaragoza		\"),"+
			"(\"	Hixquilucan		\"),"+
			"(\"	Tepotzotlán		\"),"+
			"(\"	Cuautitlán Izcalli		\"),"+
			"(\"	Zumpango		\"),"+
			"(\"	Tequisquiac		\"),"+
			"(\"	Teoloyucan		\"),"+
			"(\"	Hueypoxtla		\"),"+
			"(\"	Huehuetoca		\"),"+
			"(\"	Coyotepec		\"),"+
			"(\"	Apaxco		\"),"+
			"(\"	Tultitlán		\"),"+
			"(\"	Melchor Ocampo		\"),"+
			"(\"	Cuautitlán		\"),"+
			"(\"	Tultepec		\"),"+
			"(\"	Tonanitla		\"),"+
			"(\"	Nextlalpan		\"),"+
			"(\"	Jaltenco		\"),"+
			"(\"	Coacalco		\"),"+
			"(\"	Ecatepec		\"),"+
			"(\"	Temascalapa		\"),"+
			"(\"	Tecámac		\"),"+
			"(\"	Acolman		\"),"+
			"(\"	Axapusco		\"),"+
			"(\"	Nopaltepec		\"),"+
			"(\"	San Martín de las Pirámides		\"),"+
			"(\"	Teotihuacán		\"),"+
			"(\"	Otumba		\"),"+
			"(\"	Tezoyuca		\"),"+
			"(\"	Texcoco		\"),"+
			"(\"	Tepetlaoxtoc		\"),"+
			"(\"	Papalotla		\"),"+
			"(\"	Chiautla		\"),"+
			"(\"	Chiconcuac		\"),"+
			"(\"	Atenco		\"),"+
			"(\"	Nezahualcóyotl Zona Centro		\"),"+
			"(\"	Chimalhuacán		\"),"+
			"(\"	La Paz		\"),"+
			"(\"	Chicoloapan		\"),"+
			"(\"	Nezahualcóyotl Norte		\"),"+
			"(\"	Ixtapaluca		\"),"+
			"(\"	Chalco		\"),"+
			"(\"	Cocotitlán		\"),"+
			"(\"	Juchitepec		\"),"+
			"(\"	Temamatla		\"),"+
			"(\"	Tenango del Aire		\"),"+
			"(\"	Tlalmanalco		\"),"+
			"(\"	Amecameca		\"),"+
			"(\"	Atlautla		\"),"+
			"(\"	Ayapango		\"),"+
			"(\"	Ecatzingo		\"),"+
			"(\"	Ozumba		\"),"+
			"(\"	Tepetlixpa		\"),"+
			"(\"	Valle de Chalco Solidaridad		\");";


	String iRegion = "INSERT INTO Region (nombreRegion) VALUES"+
			"(\"	Región 1	\"),"+
			"(\"	Región 2	\"),"+
			"(\"	Región 2 A	\"),"+
			"(\"	Región 3	\"),"+
			"(\"	Región 26	\"),"+
			"(\"	Región 27	\"),"+
			"(\"	Región 4	\"),"+
			"(\"	Región 5	\"),"+
			"(\"	Región 5 A	\"),"+
			"(\"	Región 6	\"),"+
			"(\"	Región 7	\"),"+
			"(\"	Región 8	\"),"+
			"(\"	Región 8 A	\"),"+
			"(\"	Región 9	\"),"+
			"(\"	Región 9 A	\"),"+
			"(\"	Región 10	\"),"+
			"(\"	Región 12	\"),"+
			"(\"	Región 25 A	\"),"+
			"(\"	Región 11	\"),"+
			"(\"	Región 11 A	\"),"+
			"(\"	Región 25	\"),"+
			"(\"	Región 13	\"),"+
			"(\"	Región 14	\"),"+
			"(\"	Región 15	\"),"+
			"(\"	Región 15 A	\"),"+
			"(\"	Región 29	\"),"+
			"(\"	Región 16	\"),"+
			"(\"	Región 27	\"),"+
			"(\"	Región 27 A	\"),"+
			"(\"	Región 28	\"),"+
			"(\"	Región 18	\"),"+
			"(\"	Región 19 A	\"),"+
			"(\"	Región 19	\"),"+
			"(\"	Región 20	\"),"+
			"(\"	Región 21	\"),"+
			"(\"	Región 22	\"),"+
			"(\"	Región 22 A	\"),"+
			"(\"	Región 30	\"),"+
			"(\"	Región 23	\"),"+
			"(\"	Región 23 A	\"),"+
			"(\"	Región 24	\"),"+
			"(\"	Región 24 A	\");";
	String iSubdireccion = "INSERT INTO Subdireccion(nombreSubdireccion, telefono) VALUES"+
			"(\"	Toluca	\",	 \"	(722) 216-18-24	\"),"+
			"(\"	Ixtapan	\",	 \"	(01-714) 1-42-04-78	\"),"+
			"(\"	Sur	\",	 \"	(724) 267-27-82	\"),"+
			"(\"	Valle de Bravo	\",	 \"	(726) 262-06-70	\"),"+
			"(\"	Norte	\",	 \"	(761) 734-13-06	\"),"+
			"(\"	Atlacomulco	\",	 \"	(712) 122-62-45	\"),"+
			"(\"	Metropolitana	\",	 \"	(55) 55-65-21-53	\"),"+
			"(\"	Valle Cuautitlán	\",	 \"	(55) 58-70-15-96	\"),"+
			"(\"	Pirámides	\",	 \"	(55) 58-82-24-77	\"),"+
			"(\"	Texcoco	\",	 \"		\"),"+
			"(\"	Oriente	\",	 \"	(55) 57-43-27-56	\"),"+
			"(\"	Volcanes	\",	 \"	(55) 59-73-05-90	\");";
	
	String iCobertura = "INSERT INTO Cobertura (idSubdireccion, idRegion, idMunicipio) VALUES"+
"(	1	,	1	,	1	),"+
"(	1	,	2	,	2	),"+
"(	1	,	2	,	3	),"+
"(	1	,	2	,	4	),"+
"(	1	,	3	,	5	),"+
"(	1	,	3	,	6	),"+
"(	1	,	4	,	7	),"+
"(	1	,	4	,	8	),"+
"(	1	,	4	,	9	),"+
"(	1	,	4	,	10	),"+
"(	1	,	4	,	11	),"+
"(	1	,	4	,	12	),"+
"(	1	,	5	,	13	),"+
"(	1	,	5	,	14	),"+
"(	1	,	6	,	15	),"+
"(	1	,	6	,	16	),"+
"(	1	,	6	,	17	),"+
"(	2	,	7	,	18	),"+
"(	2	,	7	,	19	),"+
"(	2	,	7	,	20	),"+
"(	2	,	7	,	21	),"+
"(	2	,	7	,	22	),"+
"(	2	,	8	,	23	),"+
"(	2	,	8	,	24	),"+
"(	2	,	8	,	25	),"+
"(	2	,	9	,	26	),"+
"(	2	,	9	,	27	),"+
"(	2	,	9	,	28	),"+
"(	2	,	10	,	29	),"+
"(	2	,	10	,	30	),"+
"(	2	,	10	,	31	),"+
"(	3	,	11	,	32	),"+
"(	3	,	11	,	33	),"+
"(	3	,	11	,	34	),"+
"(	3	,	11	,	35	),"+
"(	3	,	12	,	36	),"+
"(	3	,	12	,	37	),"+
"(	3	,	12	,	38	),"+
"(	3	,	12	,	39	),"+
"(	3	,	13	,	40	),"+
"(	3	,	13	,	41	),"+
"(	4	,	14	,	42	),"+
"(	4	,	14	,	43	),"+
"(	4	,	15	,	44	),"+
"(	4	,	15	,	45	),"+
"(	4	,	15	,	46	),"+
"(	4	,	16	,	47	),"+
"(	4	,	16	,	48	),"+
"(	4	,	16	,	49	),"+
"(	4	,	16	,	50	),"+
"(	5	,	17	,	51	),"+
"(	5	,	17	,	52	),"+
"(	5	,	17	,	53	),"+
"(	5	,	17	,	54	),"+
"(	5	,	18	,	55	),"+
"(	5	,	18	,	56	),"+
"(	5	,	18	,	57	),"+
"(	5	,	18	,	58	),"+
"(	6	,	19	,	59	),"+
"(	6	,	19	,	60	),"+
"(	6	,	20	,	61	),"+
"(	6	,	20	,	62	),"+
"(	6	,	20	,	63	),"+
"(	6	,	21	,	64	),"+
"(	6	,	21	,	65	),"+
"(	6	,	21	,	66	),"+
"(	6	,	21	,	67	),"+
"(	7	,	22	,	68	),"+
"(	7	,	22	,	69	),"+
"(	7	,	22	,	70	),"+
"(	7	,	23	,	71	),"+
"(	7	,	24	,	72	),"+
"(	7	,	25	,	73	),"+
"(	7	,	26	,	74	),"+
"(	8	,	27	,	75	),"+
"(	8	,	27	,	76	),"+
"(	8	,	28	,	77	),"+
"(	8	,	28	,	78	),"+
"(	8	,	28	,	79	),"+
"(	8	,	28	,	80	),"+
"(	8	,	28	,	81	),"+
"(	8	,	28	,	82	),"+
"(	8	,	28	,	83	),"+
"(	8	,	29	,	84	),"+
"(	8	,	29	,	85	),"+
"(	8	,	29	,	86	),"+
"(	8	,	30	,	87	),"+
"(	8	,	30	,	88	),"+
"(	8	,	30	,	89	),"+
"(	8	,	30	,	90	),"+
"(	8	,	30	,	91	),"+
"(	9	,	31	,	92	),"+
"(	9	,	32	,	93	),"+
"(	9	,	32	,	94	),"+
"(	9	,	32	,	95	),"+
"(	10	,	33	,	96	),"+
"(	10	,	33	,	97	),"+
"(	10	,	33	,	98	),"+
"(	10	,	33	,	99	),"+
"(	10	,	33	,	100	),"+
"(	10	,	34	,	101	),"+
"(	10	,	34	,	102	),"+
"(	10	,	34	,	103	),"+
"(	10	,	34	,	104	),"+
"(	10	,	34	,	105	),"+
"(	10	,	34	,	106	),"+
"(	10	,	34	,	107	),"+
"(	11	,	35	,	108	),"+
"(	11	,	36	,	109	),"+
"(	11	,	37	,	110	),"+
"(	11	,	37	,	111	),"+
"(	11	,	38	,	112	),"+
"(	12	,	39	,	113	),"+
"(	12	,	40	,	114	),"+
"(	12	,	40	,	115	),"+
"(	12	,	40	,	116	),"+
"(	12	,	40	,	117	),"+
"(	12	,	40	,	118	),"+
"(	12	,	40	,	119	),"+
"(	12	,	41	,	120	),"+
"(	12	,	41	,	121	),"+
"(	12	,	41	,	122	),"+
"(	12	,	41	,	123	),"+
"(	12	,	41	,	124	),"+
"(	12	,	41	,	125	),"+
"(	12	,	42	,	126	);";
	
	SQLiteDatabase bd = getWritableDatabase();
	bd.execSQL(iMunicipio);
	bd.execSQL(iRegion);
	bd.execSQL(iSubdireccion);
	bd.execSQL(iCobertura);

	//bd.close();
}

/*public Vector<String> seleccionarBS(){
	SQLiteDatabase db = getReadableDatabase();
	Vector<String> result = new Vector<String>();
	Cursor cursor = db.rawQuery(sentenciaSelect,null);
	int i=0;
	while (cursor.moveToNext()){
		result.add(cursor.getString(i));
		i++;
	}
	cursor.close();
	return result;
}*/

public void consultaMunicipio(){/*retorna lista de objetos MunicipioManejador, cada objeto contiene un municipio*/
	//ArrayList<MunicipioManejador> datosMunicipio = new ArrayList<MunicipioManejador>();
	SQLiteDatabase bd = getReadableDatabase();
	String sentenciaSelect = "SELECT nombreMunicipio FROM Municipio ORDER BY nombreMunicipio;";
	String municipio = "";
	
	Cursor cursor = bd.rawQuery(sentenciaSelect,null);
	
	
	//int i=0;
	while (cursor.moveToNext()){
		//municipio = cursor.getString(cursor.getColumnIndex("nombreMunicipio"));
		municipio = cursor.getString(0);
				/* obtiene dato de BD    Obtiene numero columna con "nombreCampo"*/
		//result.add(cursor.getString(i));
		//i++;
		//datosMunicipio.add(new MunicipioManejador(municipio)); /*agrega nuevo objeto con datos de la BD (obtenido de la consulta SQL)*/
	}
	cursor.close();
	//return datosMunicipio;
}

	public ArrayList getMunicipios(){/*retorna lista de objetos MunicipioManejador, cada objeto contiene un municipio*/
		ArrayList<Municipio> datosMunicipio = new ArrayList<Municipio>();
		SQLiteDatabase bd = getReadableDatabase();
		String sentenciaSelect = "SELECT nombreMunicipio FROM Municipio ORDER BY nombreMunicipio;";
		String municipio = "";

		Cursor cursor = bd.rawQuery(sentenciaSelect,null);

		while (cursor.moveToNext()){
			//municipio = cursor.getString(cursor.getColumnIndex("nombreMunicipio"));
			municipio = cursor.getString(0);
				/* obtiene dato de BD    Obtiene numero columna con "nombreCampo"*/
			datosMunicipio.add(new Municipio(municipio)); /*agrega nuevo objeto con datos de la BD (obtenido de la consulta SQL)*/
		}
		cursor.close();
		return datosMunicipio;
	}


}