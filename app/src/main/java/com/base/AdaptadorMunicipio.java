package com.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

/**
 * Created by stn on 18/09/16.
 * Clase adaptador para crear la vista de cada elemento de la lista con la  vista de cada elemento: datos_municipio.xml
 */
public class AdaptadorMunicipio extends RecyclerView.Adapter<AdaptadorMunicipio.ViewHolder>{
    private List<Municipio> municipios;   //lista municipios de tipo Municipio que almacenara la informacion a mostrar en lista

    public static class ViewHolder extends RecyclerView.ViewHolder {    //Clase estatica para hacer referencia a los elementos del xml
        public TextView textNombreMunicipio;
        public Button botonMunicipio;

        public ViewHolder(View itemView) {
            super(itemView);
            textNombreMunicipio = (TextView) itemView.findViewById(R.id.editTextMunicipio);
            botonMunicipio = (Button) itemView.findViewById(R.id.buttonMunicipio);
        }
    }

    AdaptadorMunicipio(List<Municipio> listaMunicipios){        //Constructor para definir la lista a mostrar
        this.municipios = listaMunicipios;
    }


    @Override
    public AdaptadorMunicipio.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {    //para inflar este ViewHolder con el layout que mostrara un item en la lista
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View municipioView = inflater.inflate(R.layout.datos_municipio, parent, false);   //pasa como parametro el layout para mostrar un elemento municipio

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(municipioView);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(AdaptadorMunicipio.ViewHolder holder, int position) {
        Municipio municipio = municipios.get(position);         //obtener informacion en dicha posicion

        TextView textView = holder.textNombreMunicipio;         //colocar valores a los elementos de la vista
        textView.setText(municipio.getMunicipios());

        Button boton = holder.botonMunicipio;
        boton.setText(municipio.getMunicipios());

    }


    @Override
    public int getItemCount() {
        return municipios.size();
    }
}
