package com.base;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;


public class MainActivity extends Activity {
	//private String sentenciaInsertar = "INSERT INTO BasePrueba.Municipios (nombreMunicipio) VALUES (\"Otumba\");";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        CrearBD base = new CrearBD(this, "BasePrueba", null, 1);
        
       // base.insertarDatos();
       // base.consultaMunicipio();
								/*contexto,nombre, factory,version*/
        SQLiteDatabase db = base.getWritableDatabase();

        if(db != null){

            base.insertarDatos();
        }
        db.close();
    }
    public void lanzaConsultaMunicipio(View view){
        Intent i = new Intent(this, CrearVistaMunicipio.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
   /* public void crearBase(){
    	CrearBD base = new CrearBD(this, "BasePrueba", null, 1);
    								//nombre, factory,version
    	SQLiteDatabase db = base.getWritableDatabase();
    	
    	 if(db != null){
    		 db.execSQL(sentenciaInsertar);	//metodo insertar datos
    	 }
    	db.close();
    }*/
    
    /*public void insertarBase(View view){
    	CrearBD usdbh = new CrearBD(this, "BasePrueba", null, 1);
    								
    	SQLiteDatabase db = usdbh.getWritableDatabase();
    	 if(db != null){
    		 usdbh.insertarBD();	//metodo insertar datos
    	 }
    	 db.close();
    }*/
    
   /* public void selectBase(View view){
    	CrearBD usdbh = new CrearBD(this, "BasePrueba", null, 1);
    								
    	Vector<String> resultado = new Vector<String>();
    	
    	SQLiteDatabase db = usdbh.getReadableDatabase();
    	resultado = usdbh.seleccionarBS();	//metodo insertar datos
    	db.close();
    	
    	((EditText) findViewById(R.id.textView1)).setText((CharSequence) resultado);
    	
    	
    }*/
    
}
